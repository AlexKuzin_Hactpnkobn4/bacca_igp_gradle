package com.example.gradle_learning;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GradleLearningApplication {

    public static void main(String[] args) {
        SpringApplication.run(GradleLearningApplication.class, args);
    }

}
